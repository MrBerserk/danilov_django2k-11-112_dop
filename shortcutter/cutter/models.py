from django.db import models

# Create your models here.
class ShortedUrl(models.Model):
    original_url = models.URLField()
    title = models.CharField(max_length=1024, blank=True)
    hashed = models.CharField(max_length=36, unique=True, blank=True)
    clicks = models.PositiveIntegerField(default=0)
