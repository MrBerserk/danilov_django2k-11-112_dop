import uuid

from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, Http404, HttpResponseRedirect

# Create your views here.
from .models import ShortedUrl
from .forms import ShortenerForm

import requests
from bs4 import BeautifulSoup


def main_view(request):
    template = 'cutter/main.html'
    context = {'form': ShortenerForm()}

    if request.method == 'GET':
        return render(request, template, context)
    elif request.method == 'POST':
        form = ShortenerForm(request.POST)
        if form.is_valid():
            title = 'Без названия'
            try:
                soup = BeautifulSoup(requests.get('form.original_url').text, 'html.parser')
                if soup.title is not None:
                    title = soup.title.text
            except:
                title = 'Ошибка обработки сайта'

            hashed = str(uuid.uuid4())
            shorted_link = ShortedUrl(original_url=form.original_url, hashed=hashed, title=title).save()

            new_url = request.build_absolute_uri('/') + shorted_link.hashed
            return render(request, template, context)

        context['errors'] = form.errors
        return render(request, template, context)


def redirect_url_view(request, hashed):
    try:
        shortener = ShortedUrl.objects.get(hashed=hashed)
        shortener.clicks += 1
        shortener.save()
        return HttpResponseRedirect(shortener.original_url)
    except:
        raise Http404('Запрашиваемая ссылка еще не существует')
