from django import forms

from .models import ShortedUrl

class ShortenerForm(forms.ModelForm):
    original_url = forms.URLField()
    class Meta:
        model = ShortedUrl
        fields = ('original_url', )
