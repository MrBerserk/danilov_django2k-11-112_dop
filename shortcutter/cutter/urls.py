from django.urls import path

from .views import main_view, redirect_url_view

appname = "cutter"

urlpatterns = [
    path("", main_view, name="main"),
    path('<str:hashed>', redirect_url_view, name='redirect')
]